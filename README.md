# RT-Extension-QueueChangeOnUpdate

An extension (plugin) for Request Tracker that will allow you to move a ticket to a different queue when commenting or replying to a ticket. This extension works really well with [RT::Extension::CustomFieldsOnUpdate](https://metacpan.org/pod/RT::Extension::CustomFieldsOnUpdate).

- [Blog article: Request Tracker extension queue change on update (Zenbu Tech)](https://zenbu.tech/request-tracker-extension-queue-change-on-update/)

## Overview

Installation is done by creating a few directories for the plugin and adding the files needed to do the magic and have the ability to enable/disable this as a plugin.

## Requirements

- This has been tested with Request Tracker 4.4, but will probably also work on version 5.

## Usage

Make sure these variables are correct:

```bash
# RT installation path
RT_DIR="/opt/rt4"

# Directories needed for the RT::Extension::QueueChangeOnUpdate extension
UPDATEHTML_DIR="local/plugins/RT-Extension-QueueChangeOnUpdate/html/Callbacks/QueueChangeOnUpdate/Ticket/Update.html"
EXTENSION_DIR="local/plugins/RT-Extension-QueueChangeOnUpdate/lib/RT/Extension"
LIB_DIR="local/plugins/RT-Extension-QueueChangeOnUpdate/lib"
```

Then copy the script to your RT server and run it using sudo:

```bash
$ sudo ./install_extension_queuechangeonupdate.sh
```

## License

MIT License, see `LICENSE` file for more details.
