#!/bin/bash

#
# Author  : Bart 全部技術
# Version : 1.0.0
# License : MIT License
#


#
# Variables (make sure these are correct)
#

# RT installation path
RT_DIR="/opt/rt4"

# Directories needed for the RT::Extension::QueueChangeOnUpdate extension
UPDATEHTML_DIR="local/plugins/RT-Extension-QueueChangeOnUpdate/html/Callbacks/QueueChangeOnUpdate/Ticket/Update.html"
EXTENSION_DIR="local/plugins/RT-Extension-QueueChangeOnUpdate/lib/RT/Extension"
LIB_DIR="local/plugins/RT-Extension-QueueChangeOnUpdate/lib"


#
# Create RT::Extension::QueueChangeOnUpdate plugin directories
#

mkdir -p $RT_DIR/$UPDATEHTML_DIR
mkdir -p $RT_DIR/$EXTENSION_DIR
mkdir -p $RT_DIR/$LIB_DIR


#
# Create the AfterWorked file
# This is the part that will load the drop down for selecting a different queue.
#

echo '<tr>
    <td class="label">Queue:</td>
    <td>
        <& /Elements/SelectQueue,
            Name => "Queue",
            Default => $Ticket->QueueObj->Id,
            InTable => 1,
        &>
    </td>
</tr>
<%ARGS>
$Ticket
</%ARGS>' > $RT_DIR/$UPDATEHTML_DIR/AfterWorked


#
# Create the perllocal.pod file
#

echo '=head2 Mon Feb 13 09:52:24 2012: C<Module> L<RT::Extension::QueueChangeOnUpdate|RT::Extension::QueueChangeOnUpdate>

=over 4

=item *

C<installed into: /opt/rt4/local/plugins/RT-Extension-QueueChangeOnUpdate/lib>

=item *

C<LINKTYPE: dynamic>

=item *

C<VERSION: 0.01>

=item *

C<EXE_FILES: >

=back

=head2 Mon Feb 13 11:52:17 2012: C<Module> L<RT::Extension::QueueChangeOnUpdate|RT::Extension::QueueChangeOnUpdate>

=over 4

=item *

C<installed into: /opt/rt4/local/plugins/RT-Extension-QueueChangeOnUpdate/lib>

=item *

C<LINKTYPE: dynamic>

=item *

C<VERSION: 0.01>

=item *

C<EXE_FILES: >

=back' > $RT_DIR/$LIB_DIR/perllocal.pod


#
# Create the QueueChangeOnUpdate.pm file
#

echo "use 5.008003;
use strict;
use warnings;

package RT::Extension::QueueChangeOnUpdate;

our \$VERSION = '0.01';

=head1 NAME

RT::Extension::QueueChangeOnUpdate - edit ticket's Queue on reply/comment

=head1 DESCRIPTION

This extension adds often requested feature - update of ticket's Queue on
reply and comment.

This is for RT 4.4.x.

=head1 INSTALLATION

To install run the script, it'll create all the files and directories.

Register 'RT::Extension::QueueChangeOnUpdate' in the site config;

    Set(@Plugins, qw(
        RT::Extension::QueueChangeOnUpdate
        ... other plugins you may have ...
    ));

If you're also using RT::Extension::CustomFieldsOnUpdate then make sure
you include the QueueChangeOnUpdate plugin before the CustomFieldsOnUpdate.

    Set(@Plugins, qw(
        RT::Extension::QueueChangeOnUpdate
        RT::Extension::CustomFieldsOnUpdate
        ... other plugins you may have ...
    ));

=cut

=head1 AUTHOR

Bart E<lt>bart@pleh.infoE<gt>

=head1 LICENSE

Under the same terms as perl itself.

=cut

1;" > $RT_DIR/$EXTENSION_DIR/QueueChangeOnUpdate.pm
